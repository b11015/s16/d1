// console.log("Hello");

let myFood = ["Shrimp", "Adobo", "Chaofan"];
console.log('My favorite food is' + ' ' + myFood);

let imSingle = true;
console.log("imSingle" + ' ' + imSingle)

let faveRestaurant = ["Chowking", "Mcdonalds", "KFC", "Gringgo", "Giligans"];
console.log(faveRestaurant);

let artist = ["Ben&ben", "IV of Spades", "Parokya ni Edgar"];
console.log("My favorite artist")
console.log(artist);

let person = {
	fullName : "Justine Bieber",
	age: 25,
	isStudent : true,
	contactNo : ["0000000000", "4444 5555"],
	address: {
		houseNumber: "California",
		city: "USA",
	birthday: "April 9, 2000",
	}
};
console.log(person);

let faveSinger = {
	fistName: "Yoongi",
	lasName: "Min",
	stageName: "Suga",
	birhDay: "March 09, 1993",
	bestAlbum: "Proof",
	bestTVShow: null,
	isActive: true
};
console.log(faveSinger);

// Operators

//  Arithmetic Operators

  let x = 1397;
  let y = 7831;

  let sum = x + y;
  console.log("Result of addition operator: " + sum);
  // Result of addition operator: 9228

  let difference = x - y;
  console.log("Result of subtraction operator: " + difference);
  // Result of subtraction operator: -6434

  let product = x * y;
  console.log("Result of multiplication operator: " + product);
  // Result of multiplication operator: 10939907

  let quotient = x / y;
  console.log("Result of division operator: " + quotient);
  // Result of division operator: 0.17839356404035245

  let remainder = y % x;
  console.log("Result of modulo operator: " + remainder);
  // Result of modulo operator846

// Assignement Operator 
	// (=) Basic Assignment Operator (=)

	let assignmentNumber = 8;

	// Shorthand arithmetic opertors

	// Addition Assigment Operator (+=)
	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition assignment:" + assignmentNumber);
	//Result of addition assignment: 10

	assignmentNumber += 2;
	console.log("Result of addition assignment:" + assignmentNumber);
	//Result of addition assignment: 12

	let string1 = "Boston";
	let string2 = "Celtics";

	string1 += string2;
	console.log(string1); // Result: BostonCeltics


	// Shorthand Method
	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber); //result: 10

	string1 -= string2;
	console.log(string1); //result: NaN (Not a Number);


	/*
		Mini-Activity
		>> Using the shorthand method, perform the following operations on the assingment number
			1.multiplaction (*)
			2. division (/)

		>>Print the individual output in your console

	*/

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber); //result: 20

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber); //result: 10

	//  Multiple Operators and Parentheses

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas: " + mdas); // Result of mdas: 0.6000000000000001
	/*
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
	*/

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas: " + pemdas); // Result of pemdas: 0.2
	/*
		1. 4 / 5 = 0.8
		2. 2 - 3 = -1
		3. -1 * 0.8 = -0.8
		4. 1 + -0.8 = 0.2
	*/

// Increment and Decrement
	// pluse 1 or minus 1 only
	//  pre-increment and post-increment
	let z = 1;

	let increment = ++z;
	console.log("Result of pre-increment: " + increment); //result: 2
	console.log("Result of pre-increment: " + z); //result: 2

	increment = z++;
	console.log("Result of post-increment: " + increment); // result: 2
	console.log("Result of post-increment: " + z); // result: 3

	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement); //result: 2
	console.log(z); //result 2

	decrement = z--;
	console.log("Result of post-decrement: " + decrement); //result: 2
	console.log(z); // result: 1

// Type Coercion

let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log("Result of coercion: " + coercion); // result: 1012
console.log(typeof coercion); // result: string

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log("nonCoercion: " + nonCoercion); //result: nonCoercion: 30

console.log(typeof nonCoercion); //result: number

let numE = true + 1;
//  true === 1
// 1 + 1 = 2
console.log(numE); //result: 2

let numF = false + 1;
// false == 0
// 0 + 1
console.log(numF); // result: 1

// Comparison Operators
	// Equality Operator (==)
	let anya = "anya";

	//Compares two strings that are the same
	console.log("anya" == "anya"); //result: true

	//Compares a string with the variable "juan" declared above
	console.log("anya" == anya); //result: true

	console.log(1 == false); //result: false
	console.log(1 == true); //result: true
	console.log("false" == false); //result: false

	// Loose Inequality Operator (!=)
	console.log(1 != 1); // result: false
	console.log("1" != 1); //result: false
	console.log(anya != "anya"); //result: false
	console.log(1 != 2); //result: true

	// Strict Equality Operatos (===)

	console.log(1 === 1); //result: true
	console.log("1" === 1) //result: false
	console.log(0 === false); //result; false

	// Strict Inequality Operators (!==)

	console.log("1" !== true); //result: true
	console.log("Anya" !== "anya"); //result: true

// Relational Operators

let a = 50;
let b = 65;

// GT or greater than operator (>)
let isGreaterThan = a > b;
console.log(isGreaterThan); //result: false

// LT or less than operator (<)
let isLessThan = a < b;
console.log(isLessThan); //result: true

// GTE and LTE or greater than or equal and less than or equal (>= or <=)

let isGTE = a >= b;
let isLTE = a <= b;

console.log(isGTE, isLTE); // result: false, true

let numStr = "30";
console.log(a > numStr); //result: true - forced type coercion to change the string to number

let str = "twenty";
console.log(b >= str); //result: false - 65 >= Nan

//  Logical Operators

	let isLegalAge = true;
	let isRegistered = false;

	//  AND operator (&&- double ampersand)
		// returns true if ALL operands are true

	let allRequirementsMet = isLegalAge && isRegistered;
	console.log(allRequirementsMet); //result: false

	// OR operator (||- double pipe)
		// Returns true IF one of the operands are true

	let someRequiremensMet = isLegalAge || isRegistered;
	console.log(someRequiremensMet); //result: true

	// NOT operator (!)
		// turns a boolean into opposite value
	let someRequiremensNotMet = !isRegistered;
	console.log(someRequiremensNotMet); //result: true

	let requiredLevel = 95
	let requiredAge = 18

	let authorization1 = isRegistered && requiredLevel === 25;
	console.log(authorization1); //result: false

	let authorization2 = isRegistered && isLegalAge && requiredLevel === 95;
	console.log(authorization2); //result: false

	let authorization3 = !isRegistered && isLegalAge && requiredLevel === 95;
	console.log(authorization3); //result: true

